QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = survey-app
TEMPLATE = app

CONFIG += c++11 warn_on

SOURCES += \
    main.cc \
    main_model.cc \
    mainwindow.cc \
    welcome_screen.cc \
    teacher/teacher_screen.cc \
    questeditor/questioneditor_screen.cc \
    questeditor/questioneditor_model.cc \
    entities/question.cc

HEADERS  += \
    main_model.h \
    mainwindow.h \
    welcome_screen.h \
    teacher/teacher_screen.h \
    questeditor/questioneditor_screen.h \
    questeditor/questioneditor_model.h \
    entities/question.h

FORMS    +=
