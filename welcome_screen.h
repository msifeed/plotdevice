#ifndef WELCOMESCREEN_H
#define WELCOMESCREEN_H

#include <QFrame>
#include <QHBoxLayout>
#include <QIntValidator>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <QWidget>

class WelcomeScreen : public QWidget {
  Q_OBJECT
 public:
  explicit WelcomeScreen(QWidget *parent = 0);

 signals:
  void survey(QString name, int id);
  void login(QString username, QString password);

 private:
  QLineEdit name;
  QLineEdit surveyid;
  QLineEdit username;
  QLineEdit password;
  QPushButton auth_btn;
  QPushButton login_btn;
};

#endif  // WELCOMESCREEN_H
