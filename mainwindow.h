#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStackedWidget>

#include "main_model.h"
#include "welcome_screen.h"
#include "teacher/teacher_screen.h"
#include "questeditor/questioneditor_screen.h"

class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  explicit MainWindow(QWidget *parent = 0);
  void setModel(MainModel *model);

 public slots:
  void showWelcomeScreen();
  void showTeacherScreen();
  void showQuestionScreen();

 private:
  MainModel *model_;

  QStackedWidget *screen_stack;
  WelcomeScreen welcome_screen;
  TeacherScreen teacher_screen;
  QuestionEditorScreen questioneditor_screen;
};

#endif  // MAINWINDOW_H
