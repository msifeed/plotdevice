#ifndef TEACHERSCREEN_H
#define TEACHERSCREEN_H

#include <QHBoxLayout>
#include <QPushButton>
#include <QVBoxLayout>
#include <QWidget>

class TeacherScreen : public QWidget {
  Q_OBJECT
 public:
  explicit TeacherScreen(QWidget *parent = 0);

 signals:
  void questionScreen();
  void backScreen();

 private:
  QPushButton question_scr;
  QPushButton survey_scr;
  QPushButton review_scr;
  QPushButton welcome_scr;
};

#endif  // TEACHERSCREEN_H
