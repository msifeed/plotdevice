#include "teacher_screen.h"

TeacherScreen::TeacherScreen(QWidget *parent) : QWidget(parent) {
  question_scr.setText("Add question");
  survey_scr.setText("Add survey");
  review_scr.setText("Review answers");
  welcome_scr.setText("Back");

  QVBoxLayout *btn_layout = new QVBoxLayout;
  btn_layout->addStretch(1);
  btn_layout->addWidget(&question_scr);
  btn_layout->addWidget(&survey_scr);
  btn_layout->addWidget(&review_scr);
  btn_layout->addStretch(1);
  btn_layout->addWidget(&welcome_scr);

  QHBoxLayout *wrap_layout = new QHBoxLayout;
  wrap_layout->addStretch(1);
  wrap_layout->addLayout(btn_layout);
  wrap_layout->addStretch(1);

  setLayout(wrap_layout);

  connect(&question_scr, &QPushButton::clicked, this, &TeacherScreen::questionScreen);
  connect(&welcome_scr, &QPushButton::clicked, this, &TeacherScreen::backScreen);
}
