#ifndef QUESTIONEDITORMODEL_H
#define QUESTIONEDITORMODEL_H

#include <QList>
#include <QObject>
#include <QString>

#include "entities/question.h"

class QuestionEditorModel : public QObject {
  Q_OBJECT
 public:
  explicit QuestionEditorModel(QObject *parent = 0);
  Question *question();

 signals:
  void questionReseted();

 public slots:
  void sendQuestion();
  void reset();

 private:
  Question *question_;
};

#endif  // QUESTIONEDITORMODEL_H
