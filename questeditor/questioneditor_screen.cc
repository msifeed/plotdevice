#include "questioneditor_screen.h"

QuestionEditorScreen::QuestionEditorScreen(QWidget *parent) : QWidget(parent) {
  freeform_check.setText("Has free-form answer");
  questiontext.setPlaceholderText("Question");
  submit_btn.setText("Submit");
  back_btn.setText("Back");
  addanswer_btn.setText("Add answer");

  QWidget *scroll_realm = new QWidget;
  answers_layout = new QVBoxLayout(scroll_realm);
  answers_layout->setContentsMargins(5, 5, 5, 5);
  answers_layout->addStretch(100);

  QHBoxLayout *submit_layout = new QHBoxLayout;
  submit_layout->setContentsMargins(0, 0, 0, 0);
  submit_layout->addWidget(&submit_btn, 1);
  submit_layout->addStretch(1);
  submit_layout->addWidget(&back_btn);

  QScrollArea *answers_scroll = new QScrollArea;
  answers_scroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  answers_scroll->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
  answers_scroll->setWidgetResizable(true);
  answers_scroll->setWidget(scroll_realm);

  QVBoxLayout *left_layout = new QVBoxLayout;
  left_layout->setContentsMargins(10, 10, 0, 10);
  left_layout->addWidget(&freeform_check);
  left_layout->addWidget(&questiontext);
  left_layout->addStretch(1);
  left_layout->addLayout(submit_layout);

  QVBoxLayout *right_layout = new QVBoxLayout;
  right_layout->setContentsMargins(0, 10, 10, 10);
  right_layout->addWidget(answers_scroll);
  right_layout->addWidget(&addanswer_btn);

  QWidget *left_container = new QWidget;
  left_container->setLayout(left_layout);

  QWidget *right_container = new QWidget;
  right_container->setLayout(right_layout);

  QSplitter *splitter = new QSplitter(Qt::Horizontal);
  splitter->addWidget(left_container);
  splitter->addWidget(right_container);
  splitter->setStretchFactor(1, 2);

  QHBoxLayout *main_layout = new QHBoxLayout(this);
  main_layout->setContentsMargins(0, 0, 0, 0);
  main_layout->addWidget(splitter);

  connect(&addanswer_btn, &QPushButton::clicked, this,
          &QuestionEditorScreen::addAnswer);
  connect(&back_btn, &QPushButton::clicked, this,
          &QuestionEditorScreen::backScreen);
}

void QuestionEditorScreen::setModel(QuestionEditorModel *model) {
  model_ = model;

  connect(model_, &QuestionEditorModel::questionReseted, this,
          &QuestionEditorScreen::connectQuestion);
  connect(&submit_btn, &QPushButton::clicked, model_,
          &QuestionEditorModel::sendQuestion);
//  connect(&back_btn, &QPushButton::clicked, model_,
//          &QuestionEditorModel::reset);

  connectQuestion();
}

void QuestionEditorScreen::connectQuestion() {
  freeform_check.setChecked(model_->question()->freeform());
  questiontext.setPlainText(model_->question()->text());
  for (AnswerOption *a : answers) {
    answers_layout->removeWidget(a);
    delete a;
  }
  answers.clear();

  freeform_check.disconnect();
  questiontext.disconnect();

  connect(model_->question(), &Question::answerRemoved, this,
          &QuestionEditorScreen::removeAnswer);
  connect(&freeform_check, &QCheckBox::stateChanged, model_->question(),
          &Question::setFreeform);
  connect(&questiontext, &QPlainTextEdit::textChanged,
          [this] { model_->question()->setText(questiontext.toPlainText()); });
}

void QuestionEditorScreen::addAnswer() {
  Answer *answer = model_->question()->addAnswer();
  AnswerOption *answer_widget = new AnswerOption(answer, this);

  connect(answer_widget, &AnswerOption::removeAnswer, model_->question(), &Question::removeAnswer);

  answers.append(answer_widget);
  answers_layout->insertWidget(answers_layout->count() - 1, answer_widget);
}

void QuestionEditorScreen::removeAnswer(int index) {
  AnswerOption *answer_widget = answers.at(index);

  answer_widget->disconnect();

  answers.removeAt(index);
  answers_layout->removeWidget(answer_widget);
  delete answer_widget;
}

AnswerOption::AnswerOption(Answer *model, QuestionEditorScreen *parent)
    : QWidget(parent) {
  model_ = model;

  remove_btn.setText("-");
  answer_text.setPlaceholderText("Answer");
  answer_text.setMaximumHeight(100);

  QVBoxLayout *btn_layout = new QVBoxLayout;
  btn_layout->setContentsMargins(0, 0, 0, 0);
  btn_layout->addWidget(&correct_check);
  btn_layout->addWidget(&remove_btn);
  btn_layout->addStretch(1);

  QHBoxLayout *layout = new QHBoxLayout(this);
  layout->setContentsMargins(0, 0, 0, 0);
  layout->addLayout(btn_layout);
  layout->addWidget(&answer_text, 1);

  connect(&correct_check, &QCheckBox::clicked, model_, &Answer::setCorrect);
  connect(&answer_text, &QPlainTextEdit::textChanged,
          [this] { model_->setText(answer_text.toPlainText()); });
  connect(&remove_btn, &QPushButton::clicked, [this] {
    removeAnswer(model_->index());
  });
}

AnswerOption::~AnswerOption() {
//  correct_check.disconnect();
//  answer_text.disconnect();
//  remove_btn.disconnect();
}
