#include "questioneditor_model.h"

#include <QDebug>

QuestionEditorModel::QuestionEditorModel(QObject *parent) : QObject(parent) {
  question_ = new Question;
}

Question *QuestionEditorModel::question() { return question_; }

void QuestionEditorModel::sendQuestion() {
  qDebug() << "Sending question" << '\n'
           << "freeform:" << question_->freeform() << '\n'
           << "text:" << question_->text() << '\n'
           << "answers:";
  for (Answer *a : *question_->answers())
    qDebug() << " " << a->index() << a->correct() << '\n'
             << " text:" << a->text();

  reset();
}

void QuestionEditorModel::reset() {
  question_->disconnect();
  for (Answer *a : *question_->answers()) a->disconnect();

  delete question_;
  question_ = new Question;

  questionReseted();
}
