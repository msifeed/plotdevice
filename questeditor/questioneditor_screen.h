#ifndef QUESTIONEDITORSCREEN_H
#define QUESTIONEDITORSCREEN_H

#include <QCheckBox>
#include <QFormLayout>
#include <QHBoxLayout>
#include <QIntValidator>
#include <QLineEdit>
#include <QList>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QScrollArea>
#include <QSplitter>
#include <QToolButton>
#include <QVBoxLayout>

#include "questioneditor_model.h"

class AnswerOption;

class QuestionEditorScreen : public QWidget {
  Q_OBJECT

 public:
  explicit QuestionEditorScreen(QWidget *parent = 0);
  void setModel(QuestionEditorModel *model);

 signals:
  void backScreen();

 public slots:
  void addAnswer();
  void removeAnswer(int index);

 private slots:
  void connectQuestion();

 private:
  QuestionEditorModel *model_;

  QVBoxLayout *answers_layout;
  QCheckBox freeform_check;
  QPlainTextEdit questiontext;
  QPushButton submit_btn;
  QPushButton back_btn;
  QPushButton addanswer_btn;
  QList<AnswerOption *> answers;
};

class AnswerOption : public QWidget {
  Q_OBJECT

 public:
  explicit AnswerOption(Answer *model, QuestionEditorScreen *parent = 0);
  ~AnswerOption();

  Answer *model_;

  QCheckBox correct_check;
  QPlainTextEdit answer_text;
  QToolButton remove_btn;

 signals:
  void correctChanged(bool correct);
  void answerChanged(QString text);
  void removeAnswer(int index);
};

#endif  // QUESTIONEDITORSCREEN_H
