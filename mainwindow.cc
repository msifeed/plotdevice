#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent) {
  screen_stack = new QStackedWidget;
  screen_stack->addWidget(&welcome_screen);
  screen_stack->addWidget(&teacher_screen);
  screen_stack->addWidget(&questioneditor_screen);

  connect(&welcome_screen, &WelcomeScreen::login, this,
          &MainWindow::showTeacherScreen);
  connect(&teacher_screen, &TeacherScreen::backScreen, this,
          &MainWindow::showWelcomeScreen);
  connect(&teacher_screen, &TeacherScreen::questionScreen, this,
          &MainWindow::showQuestionScreen);
  connect(&questioneditor_screen, &QuestionEditorScreen::backScreen, this,
          &MainWindow::showTeacherScreen);

  setCentralWidget(screen_stack);
  showWelcomeScreen();
  showQuestionScreen();  // TODO remove

  setWindowTitle("Surveynator");
  setMinimumSize(300, 160);
  resize(480, 320);
  setFocus();
}

void MainWindow::setModel(MainModel *model) {
  model_ = model;

  questioneditor_screen.setModel(model_->questioneditor());
}

void MainWindow::showWelcomeScreen() {
  screen_stack->setCurrentWidget(&welcome_screen);
}

void MainWindow::showTeacherScreen() {
  screen_stack->setCurrentWidget(&teacher_screen);
}
void MainWindow::showQuestionScreen() {
  screen_stack->setCurrentWidget(&questioneditor_screen);
}
