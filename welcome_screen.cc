#include "welcome_screen.h"

WelcomeScreen::WelcomeScreen(QWidget *parent) : QWidget(parent) {
  name.setPlaceholderText("Name");
  name.setAlignment(Qt::AlignCenter);
  surveyid.setPlaceholderText("ID");
  surveyid.setValidator(new QIntValidator(0, std::numeric_limits<int>::max()));
  surveyid.setAlignment(Qt::AlignCenter);
  username.setPlaceholderText("Username");
  password.setPlaceholderText("Password");
  password.setEchoMode(QLineEdit::Password);
  auth_btn.setText("Begin survey");
  login_btn.setText("Log in");

  QVBoxLayout *child_layout = new QVBoxLayout;
  child_layout->addWidget(&name);
  child_layout->addWidget(&surveyid);
  child_layout->addWidget(&auth_btn);

  QHBoxLayout *teacher_layout = new QHBoxLayout;
  teacher_layout->addWidget(&username);
  teacher_layout->addWidget(&password);
  teacher_layout->addWidget(&login_btn);

  // Center horizontaly
  QHBoxLayout *child_wrap = new QHBoxLayout;
  child_wrap->addStretch(1);
  child_wrap->addLayout(child_layout);
  child_wrap->addStretch(1);

  // Center horizontaly
  QHBoxLayout *teacher_wrap = new QHBoxLayout;
  teacher_wrap->addStretch(1);
  teacher_wrap->addLayout(teacher_layout);
  teacher_wrap->addStretch(1);

  QFrame *separator = new QFrame;
  separator->setFrameShape(QFrame::HLine);

  QVBoxLayout *main_layout = new QVBoxLayout;
  main_layout->setContentsMargins(0, 0, 0, 10);
  main_layout->addStretch(1);
  main_layout->addLayout(child_wrap);
  main_layout->addStretch(1);
  main_layout->addWidget(separator);
  main_layout->addLayout(teacher_wrap);

  setLayout(main_layout);

  connect(&auth_btn, &QPushButton::clicked,
          [this] { survey(this->name.text(), this->surveyid.text().toInt()); });
  connect(&login_btn, &QPushButton::clicked,
          [this] { login(this->username.text(), this->password.text()); });
}
