#ifndef QUESTION_H
#define QUESTION_H

#include <QObject>
#include <QString>

class Answer;

class Question : public QObject {
  Q_OBJECT

 public:
  bool freeform();
  QString text();
  QList<Answer *> *answers();

  Answer *addAnswer();

 signals:
  void answerRemoved(int index);

 public slots:
  void setFreeform(bool freeform);
  void setText(QString text);
  void removeAnswer(int index);

 private:
  bool freeform_;
  QString text_;
  QList<Answer *> answers_;
};

class Answer : public QObject {
  Q_OBJECT
 public:
  int index();
  bool correct();
  QString text();

 public slots:
  void setIndex(int index);
  void setCorrect(bool correct);
  void setText(QString text);

 private:
  int index_;
  bool correct_;
  QString text_;
};

#endif  // QUESTION_H
