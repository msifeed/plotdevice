#include "question.h"

bool Question::freeform() { return freeform_; }

QString Question::text() { return text_; }

QList<Answer *> *Question::answers() { return &answers_; }

Answer *Question::addAnswer() {
  Answer *answer = new Answer;
  answer->setIndex(answers_.size());

  answers_.append(answer);
  return answer;
}

void Question::removeAnswer(int index) {
  Answer *answer = answers_.at(index);

  answer->disconnect();
  answers_.removeAt(index);
  delete answer;

  // Reset indexes
  for (int i = 0; i < answers_.size(); ++i) answers_.at(i)->setIndex(i);

  answerRemoved(index);
}

void Question::setFreeform(bool freeform) { freeform_ = freeform; }

void Question::setText(QString text) { text_ = text; }

int Answer::index() { return index_; }

bool Answer::correct() { return correct_; }

QString Answer::text() { return text_; }

void Answer::setIndex(int index) { index_ = index; }

void Answer::setCorrect(bool correct) { correct_ = correct; }

void Answer::setText(QString text) { text_ = text; }
