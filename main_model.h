#ifndef MAINMODEL_H
#define MAINMODEL_H

#include <QObject>

#include "questeditor/questioneditor_model.h"

class MainModel : public QObject {
  Q_OBJECT
 public:
  explicit MainModel(QObject *parent = 0);
  QuestionEditorModel *questioneditor();

 private:
  QuestionEditorModel questioneditor_;
};

#endif  // MAINMODEL_H
