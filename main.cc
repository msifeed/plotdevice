#include <QApplication>

#include "main_model.h"
#include "mainwindow.h"

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);

  MainModel m;
  MainWindow w;

  w.setModel(&m);
  w.show();

  return a.exec();
}
